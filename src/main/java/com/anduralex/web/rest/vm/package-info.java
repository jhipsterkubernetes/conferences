/**
 * View Models used by Spring MVC REST controllers.
 */
package com.anduralex.web.rest.vm;
